/*
 * processing.h
 *
 *  Created on: 11 janv. 2021
 *      Author: robin
 */

#ifndef SRC_PROCESSING_H_
#define SRC_PROCESSING_H_

#include "./kissFFT/kiss_fft.h"
#include <stdio.h>
#include <math.h>
#include "timers/timers.h"
#include "printers/printers.h"
#include "data_file.h"


/* Mathematical macros */
#define M_PI       3.14159265358979323846
#define SFFT 1024

int processing();
void benchmark();


void HannWindow(float* window,int size);
void abs2(float* out,kiss_fft_cpx* in,int size);
void float_to_cplx(kiss_fft_cpx* out,float* in, int size_in, int size_out);
int find_max(float* in, int size);
kiss_fft_cpx square(kiss_fft_cpx in);
void shannon_energy(float* sha,float* in,int size,int wind);
int to_ppm(int pos_max,int size_fft,float Fs);
int filt(float* out,float* in,float* h, int size_in, int size_filter);

#endif /* SRC_PROCESSING_H_ */
